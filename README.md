Ce dépôt permet de lister les comptes, instances et domaines banni(e)s avec une explication. Pour toutes demande de dé-bannissement, vous pouvez créer une issues ou me contacter par mail à contact[at]ketchupma.io.

**Banlist**

Instances

| Instance | Raison |
| -------- | ------ |
| preteengirls.biz | Instance pédoporn |
| hiveway.net     | Instance violant les licences mastodon, silo à données|
| hiveway.creatodon.online | Instance violant les licences mastodon, silo à données |
| witches.town     | Instance arrêtée par son propriétaire (bloquée pour éviter les erreurs trop nombreuse) |
| social.guimik.fr | Instance arrêtée par son propriétaire (bloquée pour éviter les erreurs trop nombreuse) |
| social.treyssatvincent.fr | Instance arrêtée par son propriétaire (bloquée pour éviter les erreurs trop nombreuse) |
| waifu.social | Instance  JV.com 18-25, racisme, homophobie, appel à la haine ... |
| jorts.horse | Appel à la haine |
| gab.com | white supremacists et alt-right étatsunienne à propos et comportements haineux |
| gab.ai | white supremacists et alt-right étatsunienne à propos et comportements haineux |
| exited.eu | white supremacists et alt-right étatsunienne à propos et comportements haineux |
| developer.gab.com | white supremacists et alt-right étatsunienne à propos et comportements haineux |

Users

| User | Raison |
| -------- | ------ |
| akuna@mastodon.ketchupma.io  | Bot spammeur |
| tokurane@mastodon.ketchupma.io | Bot spammeur |

Domaines mail

| User | Raison |
| -------- | ------ |
| not-bad-serials.ru  | Adesse mail utilisée par les bots spam |
| click-to-link.ru | Adesse mail utilisée par les bots spam |